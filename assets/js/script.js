// Main Menu reveal on phones
$(function () {
	$('.menu-link').click(function (e) {
		e.preventDefault();
		
		$('.main-nav').slideToggle('fast');
		return false;
	});
});


// Responsive Slides
$(function () {
  $("#slider").responsiveSlides( {
    auto: true,
    pager: true,
    nav: true,
    speed: 200,
    timeout: 11000,
    namespace: "slides"
  });
});


$(function() {
	// CLEAR SEARCH BOXES
	$('input,textarea').each(function() {
		var default_value = this.value;
		$(this).focus(function() {
			if(this.value == default_value) {
			this.value = '';
			}
		});
		$(this).blur(function() {
			if(this.value == '') {
			this.value = default_value;
			}
		});
	});
});

// Dropkick.js
$('select.styled').dropkick();


// Google Maps
$(function() {
  var myLatlng = new google.maps.LatLng(39.628383,-84.158943);
  
  var mapOptions = {
    zoom: 10,
    center: myLatlng,
    disableDefaultUI: true,
    draggable: false,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  
  var styles = [
    {
      "stylers": [
        { "weight": 0.4 }
      ]
    },{
      "featureType": "landscape",
      "stylers": [
        { "color": "#f1f1f1" }
      ]
    },{
      "featureType": "road",
      "stylers": [
        { "lightness": 1 },
        { "hue": "#ff0008" },
        { "weight": 0.3 }
      ]
    },{
      "featureType": "poi",
      "stylers": [
        { "color": "#e8e8e8" }
      ]
    }
  ]
  
  var map = new google.maps.Map(document.getElementById("map_centerville"), mapOptions);
  
  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title:"Johnny Rocket Creative"
  });
  
  //map.setOptions({styles: styles});
});